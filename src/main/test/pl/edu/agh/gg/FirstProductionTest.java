package pl.edu.agh.gg;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static pl.edu.agh.gg.ProductionGenerator.*;

public class FirstProductionTest {

    private Graph inputGraph;
    private Node inputNode;
    private FirstProduction production;


    private void syncExecution(FirstProduction production) throws InterruptedException {
        Thread thread = new Thread(production);
        thread.start();
        thread.join();
    }

    @Before
    public void initGraph() throws InterruptedException {
        inputGraph = new SingleGraph("testGraph");
        inputNode = inputGraph.addNode(BIG_E);
        inputNode.setAttribute("label", BIG_E);
        inputNode.setAttribute(POSITION_X, 0.0f);
        inputNode.setAttribute(POSITION_Y, 0.0f);
        inputNode.setAttribute(POSITION_Z, 0.0f);
        production = new FirstProduction(inputGraph, inputNode);
        syncExecution(production);
    }

    @Test
    public void inputNodeHasDefaultPosition() throws InterruptedException {
        assertEquals(Float.valueOf(0.0f), inputNode.getAttribute(POSITION_X));
        assertEquals(Float.valueOf(0.0f), inputNode.getAttribute(POSITION_Y));
        assertEquals(Float.valueOf(0.0f), inputNode.getAttribute(POSITION_Z));
    }

    @Test
    public void inputNodeLabelIsChanged() throws InterruptedException {
        assertEquals("e", inputNode.getAttribute("label"));
    }

    @Test
    public void inputNodeHasOneINeighbour() throws InterruptedException {
        assertEquals(1, inputNode.getDegree());
        Node i = inputNode.getNeighborNodeIterator().next();
        assertEquals("I", i.getAttribute("label"));
    }

    @Test
    public void nodeIHasFourENeighbours() throws InterruptedException {
        Node i = inputNode.getNeighborNodeIterator().next();
        assertEquals(5, i.getDegree());
        int countOfENeighbours = 0;
        Iterator<Node> iter = i.getNeighborNodeIterator();
        while(iter.hasNext()){
            if(iter.next().getAttribute("label").equals(BIG_E)){
                countOfENeighbours++;
            }
        }
        assertEquals(4, countOfENeighbours);
    }

    @Test
    public void nodeNWIsCorrect() throws InterruptedException {
        Node nw = inputGraph.getNode("E_C_NW");
        assertEquals(BIG_E, nw.getAttribute("label"));
        assertEquals(3, nw.getDegree());
        assertTrue(nw.hasEdgeBetween("E_C_SW"));
        assertTrue(nw.hasEdgeBetween("E_C_NE"));
        assertTrue(nw.hasEdgeBetween("E_C"));
    }

    @Test
    public void nodeNEIsCorrect() throws InterruptedException {
        Node nw = inputGraph.getNode("E_C_NE");
        assertEquals(BIG_E, nw.getAttribute("label"));
        assertEquals(3, nw.getDegree());
        assertTrue(nw.hasEdgeBetween("E_C_NW"));
        assertTrue(nw.hasEdgeBetween("E_C_SE"));
        assertTrue(nw.hasEdgeBetween("E_C"));
    }

    @Test
    public void nodeSEIsCorrect() throws InterruptedException {
        Node nw = inputGraph.getNode("E_C_SE");
        assertEquals(BIG_E, nw.getAttribute("label"));
        assertEquals(3, nw.getDegree());
        assertTrue(nw.hasEdgeBetween("E_C_NE"));
        assertTrue(nw.hasEdgeBetween("E_C_SW"));
        assertTrue(nw.hasEdgeBetween("E_C"));
    }

    @Test
    public void nodeSWIsCorrect() throws InterruptedException {
        Node nw = inputGraph.getNode("E_C_SW");
        assertEquals(BIG_E, nw.getAttribute("label"));
        assertEquals(3, nw.getDegree());
        assertTrue(nw.hasEdgeBetween("E_C_SE"));
        assertTrue(nw.hasEdgeBetween("E_C_NW"));
        assertTrue(nw.hasEdgeBetween("E_C"));
    }

    @Test
    public void nodeNWPositionIsCorrect() throws InterruptedException {
        Node nw = inputGraph.getNode("E_C_NW");
        Node ne = inputGraph.getNode("E_C_NE");
        Node sw = inputGraph.getNode("E_C_SW");
        Node i = inputGraph.getNode("E_C");
        float iX = i.getAttribute(POSITION_X);
        float iY = i.getAttribute(POSITION_Y);
        float nwX = nw.getAttribute(POSITION_X);
        float nwY = nw.getAttribute(POSITION_Y);
        float neX = ne.getAttribute(POSITION_X);
        float swY = sw.getAttribute(POSITION_Y);
        assertTrue(nwX > neX);
        assertTrue(nwY < swY);
        assertTrue(nwX > iX);
        assertTrue(nwY < iY);
    }

    @Test
    public void nodeNEPositionIsCorrect() throws InterruptedException {
        Node nw = inputGraph.getNode("E_C_NW");
        Node ne = inputGraph.getNode("E_C_NE");
        Node se = inputGraph.getNode("E_C_SE");
        Node i = inputGraph.getNode("E_C");
        float iX = i.getAttribute(POSITION_X);
        float iY = i.getAttribute(POSITION_Y);
        float neX = ne.getAttribute(POSITION_X);
        float neY = ne.getAttribute(POSITION_Y);
        float nwX = nw.getAttribute(POSITION_X);
        float seY = se.getAttribute(POSITION_Y);
        assertTrue(neX < nwX);
        assertTrue(neY < seY);
        assertTrue(neX < iX);
        assertTrue(neY < iY);
    }

    @Test
    public void nodeSEPositionIsCorrect() throws InterruptedException {
        Node sw = inputGraph.getNode("E_C_SW");
        Node ne = inputGraph.getNode("E_C_NE");
        Node se = inputGraph.getNode("E_C_SE");
        Node i = inputGraph.getNode("E_C");
        float iX = i.getAttribute(POSITION_X);
        float iY = i.getAttribute(POSITION_Y);
        float seX = se.getAttribute(POSITION_X);
        float seY = se.getAttribute(POSITION_Y);
        float neY = ne.getAttribute(POSITION_Y);
        float swX = sw.getAttribute(POSITION_X);
        assertTrue(seX < swX);
        assertTrue(seY > neY);
        assertTrue(seX < iX);
        assertTrue(seY > iY);
    }

    @Test
    public void nodeSWPositionIsCorrect() throws InterruptedException {
        Node sw = inputGraph.getNode("E_C_SW");
        Node nw = inputGraph.getNode("E_C_NW");
        Node se = inputGraph.getNode("E_C_SE");
        Node i = inputGraph.getNode("E_C");
        float iX = i.getAttribute(POSITION_X);
        float iY = i.getAttribute(POSITION_Y);
        float seX = se.getAttribute(POSITION_X);
        float swY = se.getAttribute(POSITION_Y);
        float swX = sw.getAttribute(POSITION_X);
        float nwY = nw.getAttribute(POSITION_Y);
        assertTrue(swX > seX);
        assertTrue(swY > nwY);
        assertTrue(swX > iX);
        assertTrue(swY > iY);
    }




}
