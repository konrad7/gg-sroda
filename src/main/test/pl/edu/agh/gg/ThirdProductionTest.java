package pl.edu.agh.gg;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static pl.edu.agh.gg.ProductionGenerator.*;

/**
 * Created by Ewa on 25.03.2017.
 */
public class ThirdProductionTest {

    private Graph graph;
    private ThirdProduction production;
    private Node iLeft;
    private Node iRight;
    private Node eLeft;
    private Node eRight;
    private Node eCenter;

    private final static String mergeNodeId = "E_NW_SW_E_SW_NW_MERGED_2E";

    private void syncExecution(ThirdProduction production) throws InterruptedException {
        Thread thread = new Thread(production);
        thread.start();
        thread.join();
    }

    @Before
    public void setup() throws InterruptedException {
        graph = new SingleGraph("testGraph");
        Node topI = graph.addNode("E_C");
        topI.setAttribute(LABEL,BIG_I);
        topI.setAttribute(POSITION_X, 0.0f);
        topI.setAttribute(POSITION_Y, 3.0f);
        topI.setAttribute(POSITION_Z, 0.0f);

        Node eTopLeft = graph.addNode("E_NW");
        eTopLeft.setAttribute(LABEL,SMALL_E);
        eTopLeft.setAttribute(POSITION_X, -2.0f);
        eTopLeft.setAttribute(POSITION_Y, 4.0f);
        eTopLeft.setAttribute(POSITION_Z, 0.0f);

        Node eTopRight = graph.addNode("E_SW");
        eTopRight.setAttribute(LABEL,SMALL_E);
        eTopRight.setAttribute(POSITION_X, 2.0f);
        eTopRight.setAttribute(POSITION_Y, 4.0f);
        eTopRight.setAttribute(POSITION_Z, 0.0f);

        iLeft = graph.addNode("E_NW_C");
        iLeft.setAttribute(LABEL,BIG_I);
        iLeft.setAttribute(POSITION_X, -3.0f);
        iLeft.setAttribute(POSITION_Y, 1.0f);
        iLeft.setAttribute(POSITION_Z, 0.0f);

        iRight = graph.addNode("E_SW_C");
        iRight.setAttribute(LABEL,BIG_I);
        iRight.setAttribute(POSITION_X, 3.0f);
        iRight.setAttribute(POSITION_Y, 1.0f);
        iRight.setAttribute(POSITION_Z, 0.0f);

        eLeft = graph.addNode("E_NW_SW");
        eLeft.setAttribute(LABEL,BIG_E);
        eLeft.setAttribute(POSITION_X, -1.0f);
        eLeft.setAttribute(POSITION_Y, 2.0f);
        eLeft.setAttribute(POSITION_Z, 0.0f);

        eRight = graph.addNode("E_SW_NW");
        eRight.setAttribute(LABEL,BIG_E);
        eRight.setAttribute(POSITION_X, 1.0f);
        eRight.setAttribute(POSITION_Y, 2.0f);
        eRight.setAttribute(POSITION_Z, 0.0f);

        eCenter = graph.addNode("E_NW_SE_E_NE_SW_E_SW_NE_E_SE_NW_MERGED_4E");
        eCenter.setAttribute(LABEL,BIG_E);
        eCenter.setAttribute(POSITION_X, 0.0f);
        eCenter.setAttribute(POSITION_Y, 0.0f);
        eCenter.setAttribute(POSITION_Z, 0.0f);

        Edge e1 = graph.addEdge("e1", eTopLeft, topI);
        Edge e2 = graph.addEdge("e2", eTopRight, topI);
        Edge e3 = graph.addEdge("e3", eTopLeft, iLeft);
        Edge e4 = graph.addEdge("e4", eTopRight, iRight);
        Edge e5 = graph.addEdge("e5", eLeft, iLeft);
        Edge e6 = graph.addEdge("e6", eLeft, eCenter);
        Edge e7 = graph.addEdge("e7", iLeft, eCenter);
        Edge e8 = graph.addEdge("e8", eRight, eCenter);
        Edge e9 = graph.addEdge("e9", eRight, iRight);
        Edge e10 = graph.addEdge("e10", iRight, eCenter);


        production = new ThirdProduction(graph, e5, e6, e8, e9);
        syncExecution(production);

    }


    @Test
    public void areENodesMerged(){
        assertTrue(graph.getNode(mergeNodeId) != null);
    }


    @Test
    public void newNodeIsConnected(){
        Node mergedNode = graph.getNode(mergeNodeId);
        assertTrue(mergedNode.hasEdgeBetween(eCenter.getId()));
        assertTrue(mergedNode.hasEdgeBetween(iLeft.getId()));
        assertTrue(mergedNode.hasEdgeBetween(iRight.getId()));
    }

    @Test
    public void newNodePositionIsCorrect(){
        Node mergedNode = graph.getNode(mergeNodeId);
        assertTrue(mergedNode.getAttribute(POSITION_X, Float.class) == 0.0f);
        assertTrue(mergedNode.getAttribute(POSITION_Y, Float.class) == 2.0f);
        assertTrue(mergedNode.getAttribute(POSITION_Z, Float.class) == 0.0f);
    }

    @Test
    public void oldNodesAreRemoved(){
        assertNull(graph.getNode("E_NW_SW"));
        assertNull(graph.getNode("E_SW_NW"));
    }

    @Test
    public void oldEdgesAreRemoved(){
        assertNull(graph.getEdge("e5"));
        assertNull(graph.getEdge("e6"));
        assertNull(graph.getEdge("e8"));
        assertNull(graph.getEdge("e9"));
    }


}