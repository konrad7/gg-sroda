package pl.edu.agh.gg;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.DefaultGraph;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static pl.edu.agh.gg.ProductionGenerator.*;

public class SecondProductionTest {

    private Graph graph;
    private SecondProduction production;
    private Node addedNode;

    private Node eNW;
    private Node eSW;
    private Node eSE;
    private Node eNE;
    private Node ENW;
    private Node ESW;
    private Node ESE;
    private Node ENE;
    private Node INW;
    private Node ISW;
    private Node ISE;
    private Node INE;

    private static void setAttrs(Node node, String label, float x, float y, float z) {
        node.setAttribute(LABEL, label);
        node.setAttribute(POSITION_X, x);
        node.setAttribute(POSITION_Y, y);
        node.setAttribute(POSITION_Z, z);
    }

    private void syncExecution(Production production) throws InterruptedException {
        Thread thread = new Thread(production);
        thread.start();
        thread.join();
    }

    @Before
    public void initGraph() throws InterruptedException {
        graph = new DefaultGraph("Graph");
        eNW = graph.addNode("e_NW");
        eSW = graph.addNode("e_SW");
        eSE = graph.addNode("e_SE");
        eNE = graph.addNode("e_NE");
        ENW = graph.addNode("E_NW");
        ESW = graph.addNode("E_SW");
        ESE = graph.addNode("E_SE");
        ENE = graph.addNode("E_NE");
        INW = graph.addNode("NW_I");
        ISW = graph.addNode("SW_I");
        ISE = graph.addNode("SE_I");
        INE = graph.addNode("NE_I");

        graph.addEdge("1E", eNW, eSW);
        graph.addEdge("2E", eSE, eSW);
        graph.addEdge("3E", eSE, eNE);
        graph.addEdge("4E", eNE, eNW);
        graph.addEdge("5E", eNW, INW);
        graph.addEdge("6E", eSW, ISW);
        graph.addEdge("7E", eSE, ISE);
        graph.addEdge("8E", eNE, INE);
        Edge edgeNW = graph.addEdge("9E", INW, ENW);
        Edge edgeSW = graph.addEdge("10E", ISW, ESW);
        Edge edgeSE = graph.addEdge("11E", ISE, ESE);
        Edge edgeNE = graph.addEdge("12E", INE, ENE);

        setAttrs(eNW, SMALL_E, -1, 1, 0);
        setAttrs(eSW, SMALL_E, -1, -1, 0);
        setAttrs(eSE, SMALL_E, 1, -1, 0);
        setAttrs(eNE, SMALL_E, 1, 1, 0);
        setAttrs(ENW, BIG_E, -0.5f, -4, 0);
        setAttrs(ESW, BIG_E, 0.5f, -4, 0);
        setAttrs(ESE, BIG_E, -0.5f, -5, 0);
        setAttrs(ENE, BIG_E, 0.5f, -5, 0);
        setAttrs(INW, BIG_I, -1.5f, -3, 0);
        setAttrs(INE, BIG_I, 1.5f, -3, 0);
        setAttrs(ISW, BIG_I, -1.5f, -6, 0);
        setAttrs(ISE, BIG_I, 1.5f, -6, 0);

        production = new SecondProduction(graph, edgeNW, edgeNE, edgeSW, edgeSE);
        syncExecution(production);
        addedNode = graph.getNode(production.mergedNodeId());
    }

    @Test
    public void ENodesAreRemoved() throws InterruptedException {
        assertNull(graph.getNode("E_NW"));
        assertNull(graph.getNode("E_NE"));
        assertNull(graph.getNode("E_SE"));
        assertNull(graph.getNode("E_SW"));
    }

    @Test
    public void addedNodeHasProperPosition() throws InterruptedException {
        assertEquals(Float.valueOf(0.0f), addedNode.getAttribute(POSITION_X));
        assertEquals(Float.valueOf(-4.5f), addedNode.getAttribute(POSITION_Y));
        assertEquals(Float.valueOf(0.0f), addedNode.getAttribute(POSITION_Z));
    }

    @Test
    public void addedNodeHasELabel() throws InterruptedException {
        assertEquals(BIG_E, addedNode.getAttribute(LABEL));
    }

    @Test
    public void addedNodeHasFourINeighbors() throws InterruptedException {
        assertEquals(4, addedNode.getDegree());
        assertTrue(addedNode.hasEdgeBetween(INW));
        assertTrue(addedNode.hasEdgeBetween(INE));
        assertTrue(addedNode.hasEdgeBetween(ISW));
        assertTrue(addedNode.hasEdgeBetween(ISE));
    }

    @Test
    public void eNWIsUnchanged() throws InterruptedException {
        assertEquals(3, eNW.getDegree());
        assertEquals(SMALL_E, eNW.getAttribute(LABEL));
        assertEquals(-1.0f, eNW.getAttribute(POSITION_X), 0);
        assertEquals(1.0f, eNW.getAttribute(POSITION_Y), 0);
        assertEquals(0.0f, eNW.getAttribute(POSITION_Z), 0);
        assertTrue(eNW.hasEdgeBetween(INW));
        assertTrue(eNW.hasEdgeBetween(eNE));
        assertTrue(eNW.hasEdgeBetween(eSW));
    }

    @Test
    public void eNEIsUnchanged() throws InterruptedException {
        assertEquals(3, eNE.getDegree());
        assertEquals(SMALL_E, eNE.getAttribute(LABEL));
        assertEquals(1.0f, eNE.getAttribute(POSITION_X), 0);
        assertEquals(1.0f, eNE.getAttribute(POSITION_Y), 0);
        assertEquals(0.0f, eNE.getAttribute(POSITION_Z), 0);
        assertTrue(eNE.hasEdgeBetween(INE));
        assertTrue(eNE.hasEdgeBetween(eNW));
        assertTrue(eNE.hasEdgeBetween(eSE));
    }

    @Test
    public void eSWIsUnchanged() throws InterruptedException {
        assertEquals(3, eSW.getDegree());
        assertEquals(SMALL_E, eSW.getAttribute(LABEL));
        assertEquals(-1.0f, eSW.getAttribute(POSITION_X), 0);
        assertEquals(-1.0f, eSW.getAttribute(POSITION_Y), 0);
        assertEquals(0.0f, eSW.getAttribute(POSITION_Z), 0);
        assertTrue(eSW.hasEdgeBetween(ISW));
        assertTrue(eSW.hasEdgeBetween(eSE));
        assertTrue(eSW.hasEdgeBetween(eNW));
    }

    @Test
    public void eSEIsUnchanged() throws InterruptedException {
        assertEquals(3, eSE.getDegree());
        assertEquals(SMALL_E, eSE.getAttribute(LABEL));
        assertEquals(1.0f, eSE.getAttribute(POSITION_X), 0);
        assertEquals(-1.0f, eSE.getAttribute(POSITION_Y), 0);
        assertEquals(0.0f, eSE.getAttribute(POSITION_Z), 0);
        assertTrue(eSE.hasEdgeBetween(ISE));
        assertTrue(eSE.hasEdgeBetween(eNE));
        assertTrue(eSE.hasEdgeBetween(eSW));
    }

    @Test
    public void INWIsUnchanged() throws InterruptedException {
        assertEquals(2, INW.getDegree());
        assertEquals(BIG_I, INW.getAttribute(LABEL));
        assertEquals(-1.5f, INW.getAttribute(POSITION_X), 0);
        assertEquals(-3.0f, INW.getAttribute(POSITION_Y), 0);
        assertEquals(0.0f, INW.getAttribute(POSITION_Z), 0);
        assertTrue(INW.hasEdgeBetween(eNW));
    }

    @Test
    public void INEIsUnchanged() throws InterruptedException {
        assertEquals(2, INE.getDegree());
        assertEquals(BIG_I, INE.getAttribute(LABEL));
        assertEquals(1.5f, INE.getAttribute(POSITION_X), 0);
        assertEquals(-3.0f, INE.getAttribute(POSITION_Y), 0);
        assertEquals(0.0f, INE.getAttribute(POSITION_Z), 0);
        assertTrue(INE.hasEdgeBetween(eNE));
    }

    @Test
    public void ISWIsUnchanged() throws InterruptedException {
        assertEquals(2, ISW.getDegree());
        assertEquals(BIG_I, ISW.getAttribute(LABEL));
        assertEquals(-1.5f, ISW.getAttribute(POSITION_X), 0);
        assertEquals(-6.0f, ISW.getAttribute(POSITION_Y), 0);
        assertEquals(0.0f, ISW.getAttribute(POSITION_Z), 0);
        assertTrue(ISW.hasEdgeBetween(eSW));
    }

    @Test
    public void ISEIsUnchanged() throws InterruptedException {
        assertEquals(2, ISE.getDegree());
        assertEquals(BIG_I, ISE.getAttribute(LABEL));
        assertEquals(1.5f, ISE.getAttribute(POSITION_X), 0);
        assertEquals(-6.0f, ISE.getAttribute(POSITION_Y), 0);
        assertEquals(0.0f, ISE.getAttribute(POSITION_Z), 0);
        assertTrue(ISE.hasEdgeBetween(eSE));
    }

    @Test
    public void INodesHaveTwoNeighborsEach() throws InterruptedException {
        assertEquals(2, INW.getDegree());
        assertEquals(2, INE.getDegree());
        assertEquals(2, ISE.getDegree());
        assertEquals(2, ISW.getDegree());
    }
}