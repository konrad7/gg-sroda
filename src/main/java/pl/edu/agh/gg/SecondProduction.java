package pl.edu.agh.gg;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import pl.edu.agh.gg.utils.GraphUtils;

import static pl.edu.agh.gg.ProductionGenerator.*;

public class SecondProduction extends Production {
    private final SecondProductionTriple nwTriple;
    private final SecondProductionTriple neTriple;
    private final SecondProductionTriple swTriple;
    private final SecondProductionTriple seTriple;
    public static final String MERGED_4E = "MERGED_4E";

    public SecondProduction(Graph graph,
                            Edge nwEdge,
                            Edge neEdge,
                            Edge swEdge,
                            Edge seEdge) {
        super(graph);
        this.nwTriple = initNodes(nwEdge);
        this.neTriple = initNodes(neEdge);
        this.swTriple = initNodes(swEdge);
        this.seTriple = initNodes(seEdge);
    }

    @Override
    public void run() {
        Node newNode = addNode(mergedNodeId());
        nwTriple.geteNode().getEachEdge().forEach(edge -> pinEdges(edge, nwTriple.geteNode(), newNode));
        neTriple.geteNode().getEachEdge().forEach(edge -> pinEdges(edge, neTriple.geteNode(), newNode));
        swTriple.geteNode().getEachEdge().forEach(edge -> pinEdges(edge, swTriple.geteNode(), newNode));
        seTriple.geteNode().getEachEdge().forEach(edge -> pinEdges(edge, seTriple.geteNode(), newNode));
        removeNode(nwTriple.geteNode());
        removeNode(neTriple.geteNode());
        removeNode(swTriple.geteNode());
        removeNode(seTriple.geteNode());
        newNode.setAttribute(POSITION_X, getCentrePointByDimension(POSITION_X));
        newNode.setAttribute(POSITION_Y, getCentrePointByDimension(POSITION_Y));
        newNode.setAttribute(POSITION_Z, getCentrePointByDimension(POSITION_Z));
        newNode.setAttribute(LABEL, BIG_E);
        newNode.setAttribute(UI_CLASS, BIG_E);
    }

    private void pinEdges(Edge edge, Node thrownNode, Node newNode) {
        if (edge.getNode0().equals(thrownNode)) {
            addEdge(newNode, edge.getNode1());
        } else {
            addEdge(newNode, edge.getNode0());
        }
    }

    public String mergedNodeId() {
        return nwTriple.geteNode().getId() + "_" + neTriple.geteNode().getId() + "_" + swTriple.geteNode().getId() + "_" + seTriple.geteNode().getId() + "_" + MERGED_4E;
    }

    private float getCentrePointByDimension(String dimension) {
        return GraphUtils.getQuandrangleCentreByDimension(
                nwTriple.geteNode().getAttribute(dimension, Float.class),
                neTriple.geteNode().getAttribute(dimension, Float.class),
                swTriple.geteNode().getAttribute(dimension, Float.class),
                seTriple.geteNode().getAttribute(dimension, Float.class));
    }

    private SecondProductionTriple initNodes(Edge edge) {
        Node nwNode0 = edge.getNode0();
        Node nwNode1 = edge.getNode1();
        Node iNode;
        Node eNode;
        iNode = nwNode0;
        eNode = nwNode1;
        return new SecondProductionTriple(edge, iNode, eNode);
    }

    private class SecondProductionTriple {
        private Edge edge;
        private Node iNode;
        private Node eNode;

        public SecondProductionTriple(Edge edge, Node iNode, Node eNode) {
            this.edge = edge;
            this.iNode = iNode;
            this.eNode = eNode;
        }

        public Edge getEdge() {
            return edge;
        }

        public Node getiNode() {
            return iNode;
        }

        public Node geteNode() {
            return eNode;
        }
    }
}
