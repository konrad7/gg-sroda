package pl.edu.agh.gg;


import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import pl.edu.agh.gg.utils.GraphUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ProductionGenerator {

    public static final String BIG_E = "E";
    public static final String SMALL_E = "e";
    public static final String BIG_I = "I";
    public static final String LABEL = "label";
    public static final String POSITION_X = "x";
    public static final String POSITION_Y = "y";
    public static final String POSITION_Z = "z";
    public static final int N_THREADS = 1;
    public static final String UI_CLASS = "ui.class";

    private final Graph graph;

    public ProductionGenerator(Graph graph) {
        this.graph = graph;
    }

    public void runFirstProduction() {
        runFirstProduction(getAllNodesWithLabelE());
    }

    public void runFirstProduction(Set<Node> nodes) {
        ExecutorService executor = Executors.newFixedThreadPool(N_THREADS);

        Optional.ofNullable(nodes).orElse(Collections.emptySet())
                .stream()
                .map(node -> new FirstProduction(graph, node))
                .forEach(executor::submit);

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void runSecondProduction(){
        Edge e_C_SE_C__e_C_SE_C_NW = graph.getEdge("e_C_SE_C->e_C_SE_C_NW");
        Edge e_C_SW_C__e_C_SW_C_NE = graph.getEdge("e_C_SW_C->e_C_SW_C_NE");
        Edge e_C_NW_C__e_C_NW_C_SE = graph.getEdge("e_C_NW_C->e_C_NW_C_SE");
        Edge e_C_NE_C__e_C_NE_C_SW = graph.getEdge("e_C_NE_C->e_C_NE_C_SW");

        ExecutorService executor = Executors.newFixedThreadPool(N_THREADS);

        SecondProduction secondProduction = new SecondProduction(graph,e_C_SE_C__e_C_SE_C_NW, e_C_NE_C__e_C_NE_C_SW, e_C_NW_C__e_C_NW_C_SE, e_C_SW_C__e_C_SW_C_NE);
        executor.submit(secondProduction);
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void runThirdProduction(String direction){
        ExecutorService executor = Executors.newFixedThreadPool(N_THREADS);
        Edge ieLeft;
        Edge eeLeft;
        Edge eeRight;
        Edge ieRight;


        switch (direction){
            case "N":
                ieLeft = graph.getEdge("e_C_SE_C->e_C_SE_C_SW");
                eeLeft = graph.getEdge("e_C_SE_C_NW_e_C_NE_C_SW_e_C_NW_C_SE_e_C_SW_C_NE_MERGED_4E->e_C_SE_C_SW");
                eeRight = graph.getEdge("e_C_SE_C_NW_e_C_NE_C_SW_e_C_NW_C_SE_e_C_SW_C_NE_MERGED_4E->e_C_SW_C_SE");
                ieRight = graph.getEdge("e_C_SW_C->e_C_SW_C_SE");
                break;
            case "S":
                ieLeft = graph.getEdge("e_C_NW_C->e_C_NW_C_NE");
                eeLeft = graph.getEdge("e_C_SE_C_NW_e_C_NE_C_SW_e_C_NW_C_SE_e_C_SW_C_NE_MERGED_4E->e_C_NW_C_NE");
                eeRight = graph.getEdge("e_C_SE_C_NW_e_C_NE_C_SW_e_C_NW_C_SE_e_C_SW_C_NE_MERGED_4E->e_C_NE_C_NW");
                ieRight = graph.getEdge("e_C_NE_C->e_C_NE_C_NW");
                break;
            case "E":
                ieLeft = graph.getEdge("e_C_SW_C->e_C_SW_C_NW");
                eeLeft = graph.getEdge("e_C_SE_C_NW_e_C_NE_C_SW_e_C_NW_C_SE_e_C_SW_C_NE_MERGED_4E->e_C_SW_C_NW");
                eeRight = graph.getEdge("e_C_SE_C_NW_e_C_NE_C_SW_e_C_NW_C_SE_e_C_SW_C_NE_MERGED_4E->e_C_NW_C_SW");
                ieRight = graph.getEdge("e_C_NW_C->e_C_NW_C_SW");
                break;
            case "W":
                ieLeft = graph.getEdge("e_C_NE_C->e_C_NE_C_SE");
                eeLeft = graph.getEdge("e_C_SE_C_NW_e_C_NE_C_SW_e_C_NW_C_SE_e_C_SW_C_NE_MERGED_4E->e_C_NE_C_SE");
                eeRight = graph.getEdge("e_C_SE_C_NW_e_C_NE_C_SW_e_C_NW_C_SE_e_C_SW_C_NE_MERGED_4E->e_C_SE_C_NE");
                ieRight = graph.getEdge("e_C_SE_C->e_C_SE_C_NE");
                break;
            default :
                return;

        }

        ThirdProduction thirdProduction = new ThirdProduction(graph, ieLeft, eeLeft, eeRight, ieRight);

        executor.submit(thirdProduction);
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void runSetOfFirstProductions(){
        Set<Node> nodes = new HashSet<>();
        nodes.add(graph.getNode("e_C_NE_C_NE"));
        nodes.add(graph.getNode("e_C_NW_C_NE_e_C_NE_C_NW_MERGED_2E"));
        nodes.add(graph.getNode("e_C_NW_C_NW"));
        nodes.add(graph.getNode("e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E"));
        nodes.add(graph.getNode("e_C_SW_C_SW"));

        runFirstProduction(nodes);
    }

    private void runFourthProduction(){
        ExecutorService executor = Executors.newFixedThreadPool(1);
        Edge ieTop = graph.getEdge("e_C_NW_C_NE_e_C_NE_C_NW_MERGED_2E_C->e_C_NW_C_NE_e_C_NE_C_NW_MERGED_2E_C_SE");
        Edge ieBottom = graph.getEdge("e_C_NW_C_NE_e_C_NE_C_NW_MERGED_2E_C->e_C_NW_C_NE_e_C_NE_C_NW_MERGED_2E_C_NE");
        Edge eeLeft = graph.getEdge("e_C_NE_C_NE_C_SW->e_C_NE_C_NE_C_NW");
        Edge eeRight = graph.getEdge("e_C_NW_C_NE_e_C_NE_C_NW_MERGED_2E_C_NE->e_C_NE_C_NE_C_NW->e_C_NW_C_NE_e_C_NE_C_NW_MERGED_2E_C_NE->e_C_NE_C_NE_C_SE");

        FourthProduction fourthProduction = new FourthProduction(graph, ieTop, ieBottom, eeLeft, eeRight);

        fourthProduction.run();

        ieTop = graph.getEdge("e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E_C->e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E_C_SW");
        ieBottom = graph.getEdge("e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E_C->e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E_C_SE");
        eeLeft = graph.getEdge("e_C_SW_C_SW_C_NW->e_C_SW_C_SW_C_NE");
        eeRight = graph.getEdge("e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E_C_SE->e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E_C_SW"); //boki

        fourthProduction = new FourthProduction(graph, ieTop, ieBottom, eeLeft, eeRight);

        fourthProduction.run();


        ieBottom = graph.getEdge("e_C_NW_C_NW_C->e_C_NW_C_NW_C_SE");
        ieTop = graph.getEdge("e_C_NW_C_NW_C->e_C_NW_C_NW_C_SW");
        eeRight = graph.getEdge("e_C_NW_C_NW_C_SE->e_C_NW_C_NW_C_SW");
        eeLeft = graph.getEdge("e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E_C_NW->e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E_C_NE");

        fourthProduction = new FourthProduction(graph, ieTop, ieBottom, eeLeft, eeRight);

        fourthProduction.run();

        ieTop = graph.getEdge("e_C_SW_C_NW_e_C_NW_C_SW_MERGED_2E_C_NE->e_C_NW_C_NW_C");
        ieBottom = graph.getEdge("e_C_NW_C_NW_C->e_C_NW_C_NW_C_NE");
        eeLeft = graph.getEdge("e_C_NW_C_NE_e_C_NE_C_NW_MERGED_2E_C_SW->e_C_NW_C_NE_e_C_NE_C_NW_MERGED_2E_C_NW");
        eeRight = graph.getEdge("e_C_NW_C_NW_C_NE->e_C_NW_C_NW_C_SE");

        fourthProduction = new FourthProduction(graph, ieTop, ieBottom, eeLeft, eeRight);

        fourthProduction.run();

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private Set<Node> getAllNodesWithLabelE() {
        Collection<Node> allNodes = graph.getNodeSet();
        return Optional.ofNullable(allNodes).orElse(Collections.emptySet())
                .stream()
                .filter(node -> BIG_E.equals(node.getAttribute(LABEL, String.class)))
                .collect(Collectors.toSet());
    }

    public static void main(String args[]) throws InterruptedException {
            System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
            Graph graph = GraphUtils.createDefaultGraph();
            Node node = graph.addNode("e");
            node.setAttribute(LABEL, BIG_E);
            node.setAttribute("ui.class", BIG_E);
            node.setAttribute(POSITION_X, 0.0f);
            node.setAttribute(POSITION_Y, 0.0f);
            node.setAttribute(POSITION_Z, 0.0f);

            ProductionGenerator generator = new ProductionGenerator(graph);
            generator.runFirstProduction();
            generator.runFirstProduction();

            generator.runSecondProduction();

            generator.runThirdProduction("N");
            generator.runThirdProduction("S");
            generator.runThirdProduction("E");
            generator.runThirdProduction("W");

            generator.runSetOfFirstProductions();
            generator.runFourthProduction();

            GraphUtils.exportGraph(graph, "graph.dsg");

            graph.display(false);
    }
}
