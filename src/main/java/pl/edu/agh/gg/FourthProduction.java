package pl.edu.agh.gg;

import static pl.edu.agh.gg.ProductionGenerator.POSITION_X;
import static pl.edu.agh.gg.ProductionGenerator.POSITION_Y;
import static pl.edu.agh.gg.ProductionGenerator.POSITION_Z;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import pl.edu.agh.gg.utils.GraphUtils;

public class FourthProduction extends Production {
    private Node eTopLeft;
    private Node eBottomLeft;
    private Node eTopRight;
    private Node eBottomRight;
    private Node iRight;
    private Edge ieTop;
    private Edge ieBottom;
    private Edge eeRight;

    public FourthProduction(Graph graph, Edge ieTop, Edge ieBottom, Edge eeLeft, Edge eeRight) {
        super(graph);
        initNodes(ieTop, ieBottom, eeLeft, eeRight);
    }

    @Override
    public void run() {
        setPosition(eTopLeft, eTopRight);
        setPosition(eBottomLeft, eBottomRight);
        eTopRight.getEachEdge().forEach(edge -> pinEdges(edge, eTopRight, eTopLeft));
        eBottomRight.getEachEdge().forEach(edge -> pinEdges(edge, eBottomRight, eBottomLeft));
        removeEdge(ieTop);
        removeEdge(ieBottom);
        removeEdge(eeRight);
        removeNode(eTopRight);
        removeNode(eBottomRight);
    }

    private void pinEdges(Edge edge, Node toBeRemovedNode, Node newNode) {
        if (edge.getNode0().equals(toBeRemovedNode)) {
            if (!newNode.hasEdgeBetween(edge.getNode1().getId())){
                addEdge(newNode, edge.getNode1());
            }
        } else {
            if (!newNode.hasEdgeBetween(edge.getNode0().getId())){
                addEdge(newNode, edge.getNode0());
            }
        }
    }

    private void setPosition(Node left, Node right) {
        float leftX = left.getAttribute(POSITION_X, Float.class);
        float leftY = left.getAttribute(POSITION_Y, Float.class);
        float leftZ = left.getAttribute(POSITION_Z, Float.class);
        float rightX = right.getAttribute(POSITION_X, Float.class);
        float rightY = right.getAttribute(POSITION_Y, Float.class);
        float rightZ = right.getAttribute(POSITION_Z, Float.class);

        left.setAttribute(POSITION_X, leftX + GraphUtils.calculateCentreByDimension(leftX, rightX));
        left.setAttribute(POSITION_Y, leftY + GraphUtils.calculateCentreByDimension(leftY, rightY));
        left.setAttribute(POSITION_Z, leftZ + GraphUtils.calculateCentreByDimension(leftZ, rightZ));
    }

    private void initNodes(Edge ieTop, Edge ieBottom, Edge eeLeft, Edge eeRight) {
        this.ieTop = ieTop;
        this.ieBottom = ieBottom;
        this.eeRight = eeRight;

        if (ieTop.getNode0().getId().endsWith("C")) {
            this.iRight = ieTop.getNode0();
            this.eTopRight = ieTop.getNode1();
        } else {
            this.iRight = ieTop.getNode1();
            this.eTopRight = ieTop.getNode0();
        }

        if (ieBottom.getNode0().getId().endsWith("C")) {
            this.eBottomRight = ieBottom.getNode1();
        } else {
            this.eBottomRight = ieBottom.getNode0();
        }

        this.eTopLeft = eeLeft.getNode0();
        this.eBottomLeft = eeLeft.getNode1();
    }
}