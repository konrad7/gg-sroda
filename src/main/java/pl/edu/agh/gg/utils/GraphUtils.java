package pl.edu.agh.gg.utils;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.stream.file.FileSink;
import org.graphstream.stream.file.FileSinkDGS;
import org.graphstream.stream.file.FileSource;
import org.graphstream.stream.file.FileSourceDGS;

import java.io.IOException;

public class GraphUtils {

    public static float getQuandrangleCentreByDimension(float nwValue, float neValue, float swValue, float seValue) {
        float bottomCentre = swValue + (seValue - swValue) / 2;
        float upperCentre = nwValue + (neValue - nwValue) / 2;
        return bottomCentre + (upperCentre - bottomCentre) / 2;
    }

    public static float calculateCentreByDimension(float coord1, float coord2) {
        return (coord2 - coord1) / 2.0f;
    }

    public static Graph createDefaultGraph() {
        final Graph graph = new DefaultGraph("Graph");
        graph.addAttribute("ui.stylesheet",
                "node {\n" +
                "    text-mode: normal;\n" +
                "    text-padding: 10px;\n" +
                "    text-alignment: center;\n" +
                "    text-style: bold;\n" +
                "    text-size: 20px;\n" +
                "    text-font: Arial;\n" +
                "    size-mode: fit;\n" +
                "}\n" +
                "node.E {\n" +
                "    fill-color: green;\n" +
                "}\n" +
                "\n" +
                "node.I {\n" +
                "    fill-color: red;\n" +
                "}\n" +
                "\n" +
                "node.e {\n" +
                "    fill-color: blue;\n" +
                "}");
        return graph;
    }

    public static void exportGraph(Graph graph, String filename) {
        FileSink fileSink = new FileSinkDGS();
        try {
            fileSink.writeAll(graph, filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Graph loadGraph(String filename) {
        final Graph graph = createDefaultGraph();
        FileSource fs = new FileSourceDGS();
        fs.addSink(graph);
        try {
            fs.readAll(filename);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fs.removeSink(graph);
        }
        return graph;
    }
}
