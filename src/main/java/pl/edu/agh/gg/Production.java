package pl.edu.agh.gg;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Node;

public abstract class Production implements Runnable {

    protected final Graph graph;

    public Production(Graph graph) {
        this.graph = graph;
    }

    @Override
    public abstract void run();

    protected void addEdge(Node n1, Node n2) {
        synchronized (graph) {
            graph.addEdge(n1.getId() + "->" + n2.getId(), n1, n2);
        }
    }

    protected Node addNode(String nodeId) {
        synchronized (graph) {
            return graph.addNode(nodeId);
        }
    }

    protected Node removeNode(Node node) {
        synchronized (graph) {
            return graph.removeNode(node);
        }
    }

    protected Edge removeEdge(Edge edge) {
        synchronized (graph) {
            return graph.removeEdge(edge);
        }
    }
}
