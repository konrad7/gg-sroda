package pl.edu.agh.gg;


import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import static pl.edu.agh.gg.ProductionGenerator.*;

public class FirstProduction extends Production {

    private final Node mainNode;

    public FirstProduction(Graph graph, Node node) {
        super(graph);
        this.mainNode = node;
    }

    @Override
    public void run() {
        mainNode.setAttribute(LABEL, SMALL_E);
        mainNode.addAttribute(UI_CLASS, SMALL_E);

        Node childNode = addNewNode(mainNode, "C", BIG_I, 0.0f, 0.0f, -1.0f);
        float factor = 1.0f / childNode.getAttribute(POSITION_Z, Float.class);

        Node nodeNW = addNewNode(childNode, "NW", BIG_E, -factor, factor, 0.0f);
        Node nodeNE = addNewNode(childNode, "NE", BIG_E, factor, factor, 0.0f);
        Node nodeSW = addNewNode(childNode, "SW", BIG_E, -factor, -factor, 0.0f);
        Node nodeSE = addNewNode(childNode, "SE", BIG_E, factor, -factor, 0.0f);

        addEdge(nodeNW, nodeNE);
        addEdge(nodeNE, nodeSE);
        addEdge(nodeSE, nodeSW);
        addEdge(nodeSW, nodeNW);
    }

    private Node addNewNode(Node node, String newNodeId, String label, float x, float y, float z) {
        Node newNode = addNode(node.getId() + "_" + newNodeId);
        newNode.setAttribute(LABEL, label);
        newNode.setAttribute(UI_CLASS, label);

        newNode.setAttribute(POSITION_X, node.getAttribute(POSITION_X, Float.class) + x);
        newNode.setAttribute(POSITION_Y, node.getAttribute(POSITION_Y, Float.class) + y);
        newNode.setAttribute(POSITION_Z, node.getAttribute(POSITION_Z, Float.class) + z);
        addEdge(node, newNode);
        return newNode;
    }

}
