package pl.edu.agh.gg;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static pl.edu.agh.gg.ProductionGenerator.*;

/**
 * Created by shimmy on 3/28/17.
 */
public class FourthProductionTest {
    private Graph graph;
    private FourthProduction production;

    private void syncExecution(FourthProduction production) throws InterruptedException {
        Thread thread = new Thread(production);
        thread.start();
        thread.join();
    }

    @Before
    public void initGraph() throws InterruptedException {
        graph = new SingleGraph("testGraph");

        Node rightI = graph.addNode("RIGHT_I");
        rightI.setAttribute(LABEL, BIG_I);
        rightI.setAttribute(POSITION_X, 2.0f);
        rightI.setAttribute(POSITION_Y, 0.0f);
        rightI.setAttribute(POSITION_Z, 0.0f);

        Node topLeftE = graph.addNode("TOP_LEFT_E");
        topLeftE.setAttribute(LABEL, BIG_E);
        topLeftE.setAttribute(POSITION_X, -1.0f);
        topLeftE.setAttribute(POSITION_Y, 2.0f);
        topLeftE.setAttribute(POSITION_Z, 0.0f);

        Node bottomLeftE = graph.addNode("BOTTOM_LEFT_E");
        bottomLeftE.setAttribute(LABEL, BIG_E);
        bottomLeftE.setAttribute(POSITION_X, -1.0f);
        bottomLeftE.setAttribute(POSITION_Y, -2.0f);
        bottomLeftE.setAttribute(POSITION_Z, 0.0f);

        Node topRightE = graph.addNode("TOP_RIGHT_E");
        topRightE.setAttribute(LABEL, BIG_E);
        topRightE.setAttribute(POSITION_X, 1.0f);
        topRightE.setAttribute(POSITION_Y, 2.0f);
        topRightE.setAttribute(POSITION_Z, 0.0f);

        Node bottomRightE = graph.addNode("BOTTOM_RIGHT_E");
        bottomRightE.setAttribute(LABEL, BIG_E);
        bottomRightE.setAttribute(POSITION_X, 1.0f);
        bottomRightE.setAttribute(POSITION_Y, -2.0f);
        bottomRightE.setAttribute(POSITION_Z, 0.0f);

        Edge ieTop = graph.addEdge("ieTop", rightI, topRightE);
        Edge ieBottom = graph.addEdge("ieBottom", rightI, bottomRightE);
        Edge eeLeft = graph.addEdge("eeLeft", topLeftE, bottomLeftE);
        Edge eeRight = graph.addEdge("eeRight", topRightE, bottomRightE);

        production = new FourthProduction(graph, ieTop, ieBottom, eeLeft, eeRight);
        syncExecution(production);
    }

    @Test
    public void correctPartsRemoved() {
        assertNull(graph.getEdge("ieTop"));
        assertNull(graph.getEdge("ieBottom"));
        assertNull(graph.getEdge("eeRight"));
        assertNull(graph.getNode("TOP_RIGHT_E"));
    }

    @Test
    public void areENodesMerged() {
        assertNotNull(graph.getNode("BOTTOM_LEFT_E"));
        assertNotNull(graph.getNode("TOP_LEFT_E"));
        assertNull(graph.getNode("TOP_RIGHT_E"));
        assertNull(graph.getNode("BOTTOM_RIGHT_E"));
    }

    @Test
    public void mergedNodesPositionIsCorrect() {
        Node centeredTopE = graph.getNode("TOP_LEFT_E");
        assertEquals(centeredTopE.getAttribute(POSITION_X, Float.class), 0.0f, 0.0f);
        assertEquals(centeredTopE.getAttribute(POSITION_Y, Float.class), 2.0f, 0.0f);
        assertEquals(centeredTopE.getAttribute(POSITION_Z, Float.class), 0.0f, 0.0f);

        Node centeredBottomE = graph.getNode("BOTTOM_LEFT_E");
        assertEquals(centeredBottomE.getAttribute(POSITION_X, Float.class), 0.0f, 0.0f);
        assertEquals(centeredBottomE.getAttribute(POSITION_Y, Float.class), -2.0f, 0.0f);
        assertEquals(centeredBottomE.getAttribute(POSITION_Z, Float.class), 0.0f, 0.0f);
    }

    @Test
    public void nodesAreConnectedAOK() {
        Node centeredTopE = graph.getNode("TOP_LEFT_E");
        Node rightI = graph.getNode("RIGHT_I");
        Node centeredBottomE = graph.getNode("BOTTOM_LEFT_E");

        assertTrue(centeredTopE.hasEdgeBetween(rightI));
        assertTrue(centeredBottomE.hasEdgeBetween(rightI));
        assertTrue(centeredBottomE.hasEdgeBetween(centeredTopE));
    }
}
