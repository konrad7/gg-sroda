package pl.edu.agh.gg;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import pl.edu.agh.gg.utils.GraphUtils;

import static pl.edu.agh.gg.ProductionGenerator.*;

public class ThirdProduction extends Production {
    public static final String MERGED_2E = "MERGED_2E";
    private Node iLeft;
    private Edge ieLeft;
    private Node eLeft;
    private Edge eeLeft;
    private Node eCenter;
    private Edge eeRight;
    private Node eRight;
    private Edge ieRight;
    private Node iRight;

    public ThirdProduction(Graph graph, Edge ieLeft, Edge eeLeft, Edge eeRight, Edge ieRight) {
        super(graph);
        initNodes(ieLeft, eeLeft, eeRight, ieRight);

    }

    @Override
    public void run() {
        Node newNode = addNode(eLeft.getId() + "_" + eRight.getId() + "_" + MERGED_2E);
        newNode.setAttribute(LABEL, BIG_E);
        newNode.setAttribute(UI_CLASS, BIG_E);
        setPosition(newNode);
        eLeft.getEachEdge().forEach(edge -> pinEdges(edge, eLeft, newNode));
        eRight.getEachEdge().forEach(edge -> pinEdges(edge, eRight, newNode));
        removeEdge(ieLeft);
        removeEdge(ieRight);
        removeEdge(eeLeft);
        removeEdge(eeRight);
        removeNode(eLeft);
        removeNode(eRight);
    }

    private void pinEdges(Edge edge, Node toBeRemovedNode, Node newNode) {
        if (edge.getNode0().equals(toBeRemovedNode)) {
            if (!newNode.hasEdgeBetween(edge.getNode1().getId())){
                addEdge(newNode, edge.getNode1());
            }
        } else {
            if (!newNode.hasEdgeBetween(edge.getNode0().getId())){
                addEdge(newNode, edge.getNode0());
            }
        }
    }

    private void setPosition(Node newNode) {
        float eLeftX = eLeft.getAttribute(POSITION_X, Float.class);
        float eLeftY = eLeft.getAttribute(POSITION_Y, Float.class);
        float eLeftZ = eLeft.getAttribute(POSITION_Z, Float.class);
        float eRightX = eRight.getAttribute(POSITION_X, Float.class);
        float eRightY = eRight.getAttribute(POSITION_Y, Float.class);
        float eRightZ = eRight.getAttribute(POSITION_Z, Float.class);

        newNode.setAttribute(POSITION_X, eLeftX + GraphUtils.calculateCentreByDimension(eLeftX, eRightX));
        newNode.setAttribute(POSITION_Y, eLeftY + GraphUtils.calculateCentreByDimension(eLeftY, eRightY));
        newNode.setAttribute(POSITION_Z, eLeftZ + GraphUtils.calculateCentreByDimension(eLeftZ, eRightZ));
    }

    private void initNodes(Edge ieLeft, Edge eeLeft, Edge eeRight, Edge ieRight) {
        this.ieLeft = ieLeft;
        this.eeLeft = eeLeft;
        this.eeRight = eeRight;
        this.ieRight = ieRight;

        if (ieLeft.getNode0().getId().endsWith("C")) {
            iLeft = ieLeft.getNode0();
            eLeft = ieLeft.getNode1();
        }
        else {
            iLeft = ieLeft.getNode1();
            eLeft = ieLeft.getNode0();
        }
        if (eeLeft.getNode0().getId().endsWith(SecondProduction.MERGED_4E)) {
            eCenter = eeLeft.getNode0();
        }
        else {
            eCenter = eeLeft.getNode1();
        }
        if (ieRight.getNode0().getId().endsWith("C")) {
            iRight = ieRight.getNode0();
            eRight = ieRight.getNode1();
        }
        else {
            iRight = ieRight.getNode1();
            eRight = ieRight.getNode0();
        }
    }
}
